﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;

namespace SPtest
{
    class DataByView
    {
        private string SPSurl;

        public DataByView(string url)
        {
            SPSurl = url;
        }

        public SPListItemCollection GetDataByView(Guid idList, string idView)
        {
            using (var site = new SPSite(SPSurl))
            {
                var web = site.RootWeb;
                //haven't worked through the server
                //this is how the list and view should look like
                //if there was an opportunity to work with the server
                //that precisely would write how to address the list and representation
                var list = web.Lists.GetList(idList, true);
                var view = list.Views.GetViewByBaseViewId(idView);
                
                var titlesViewList = GetTitlesColumns(view);
                string caml = CreateCamlQuery(titlesViewList);

                SPListItemCollection items = QueryItems(list, caml);
                ShowItems(items);

                return items;
            }
        }


        // in this method, we iterate through all the column headers used in the view
        // and return them as a list
        private List<string> GetTitlesColumns(SPView view)
        {
            List<string> titlesColumns = new List<string>();

            return titlesColumns;
        }


        //in this method, we form a query for CamlQuery
        private string CreateCamlQuery(List<string> titlesList)
        {
            string camlQ = "";
            foreach (var title in titlesList)
            {
                camlQ = camlQ + "<FieldRef Name='"+ title + "' />";
            }

            return camlQ;
        }

        private SPListItemCollection QueryItems(SPList list, string vievCaml)
        {
            var query = new SPQuery();

            //here we need to specify which columns we need
            query.ViewFields = vievCaml;

            // here we specify an empty query to get all the content from the list
            // if there are more than 5 thousand items you will need to set a limit
            query.Query =
                "<Query>" +
                    "<Where>" +
                    "</Where>" +
                "</Query>";

            return list.GetItems(query);
        }

        private void ShowItems(SPListItemCollection items)
        {
            foreach (SPListItem item in items)
            {
                // we iterate through all that happened and output to the console
                Console.WriteLine();
                // representation in the console did not write (if it is necessary I can paint)
            }
        }
    }
}
